﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using ConverterLib;
using System.IO.Ports;

namespace TestConverterLib
{
    public partial class MainWindow : Window
    {
        private Converter converterLib; 
        private List<string> TargetsList = new List<string>() { "Converter" , "Switch" };
        private List<Mode> ModesList = new List<Mode>() { Mode.Master , Mode.Slave};

        public MainWindow()
        {
            InitializeComponent();
            InitFields();
        }

        private void InitFields()
        {
            Ports.ItemsSource = SerialPort.GetPortNames().ToList();
            Ports.SelectedIndex = 0;
            this.Speed.ItemsSource = new List<int>() { 110, 300, 600, 1200, 2400, 4800, 9600, 14400, 19200, 38400, 56000, 57600, 115200, 128000, 256000 };
            this.Speed.SelectedIndex = 0;

            TargetList.ItemsSource = TargetsList;
            TargetList.SelectedIndex = 0;
            ModeList.ItemsSource = ModesList;
            ModeList.SelectedIndex = 0;
        }

        public void DispatchIfNecessary(Action action)
        {
            if (!Dispatcher.CheckAccess())
                Dispatcher.Invoke(action);
            else
                action.Invoke();
        }

        DateTime begin;
        DateTime end;
            

        private void SubscribeToEvents()
        {
            converterLib.OnOpenPort += (sender, zero) => {
                DispatchIfNecessary(() =>
                { tbxResult.Text += "\n" + "OpenPort"; }); };
            converterLib.OnClosePort += (sender, zero) => {
                DispatchIfNecessary(() =>
                { tbxResult.Text += "\n" + "ClosePort"; });
            };
            converterLib.OnReadBytes += (sender, data) => {
                DispatchIfNecessary(() =>
                { tbxResult.Text += "\n" + "Get data: " + BitConverter.ToString(data).Replace("-", " "); });
            };
            converterLib.OnWriteBytes += (sender, data) => {
                DispatchIfNecessary(() =>
                { tbxResult.Text += "\n" + "Send data: " + BitConverter.ToString(data).Replace("-", " "); });
            };
        }

        

        private void OpenPort_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (converterLib != null)
                {
                    converterLib.ClosePort();
                    btnPort.Content = "Open port";
                    converterLib = null;
                }
                else
                {
                    converterLib = new Converter((Mode) this.ModeList.SelectedItem);
                    SubscribeToEvents();
                    converterLib.OpenPort(Ports.SelectedItem.ToString(), (int)this.Speed.SelectedItem);
                    btnPort.Content = "Close port";

                }
                //if (relayLib != null)
                //{
                //    relayLib.ClosePort();
                //    btnPort.Content = "Open port";
                //    relayLib = null;
                //}
                //else
                //{
                //    relayLib = new Relay();
                //    SubscribeToEvents();
                //    relayLib.OpenPort(Ports.SelectedItem.ToString(), 9600);
                //    btnPort.Content = "Close port";

                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        Relay relayLib;

        private async void Send_Click(object sender, RoutedEventArgs e)
        {
            //if (converterLib == null)
            //    if (relayLib == null)
            //        return;
            //bool result;
            //relayLib.AwaiterTime = 1000;
            //result = await relayLib.SetRangeFrom0To1GHz();            
            //DispatchIfNecessary(() =>
            //{ tbxResult.Text += "\n" + result; });
            if (converterLib == null)
                return;
            bool? result = null;
            converterLib.AwaiterTime = 3000;
            try
            {
                if (TargetList.SelectedItem.ToString() == TargetsList[0])
                {
                    if ((Mode)ModeList.SelectedItem == ModesList[1])
                        result = converterLib.SetConverterFrequency(Convert.ToInt16(tbxFrequecy.Text));
                    if ((Mode)ModeList.SelectedItem == ModesList[0])
                        result = converterLib.SetConverterFrequency(Convert.ToInt16(tbxFrequecy.Text));
                }

                if (TargetList.SelectedItem.ToString() == TargetsList[1])
                {
                    if ((Mode)ModeList.SelectedItem == ModesList[1])
                        converterLib.SetCommutatorAntenna(Convert.ToByte(tbxAntenna.Text), Convert.ToByte(tbxAttenuator.Text));
                    if ((Mode)ModeList.SelectedItem == ModesList[0])
                        converterLib.SetCommutatorAntenna(Convert.ToByte(tbxAntenna.Text), Convert.ToByte(tbxAttenuator.Text));
                }
            }
            catch (Exception ex)
            { var t = ex.Message; }

            DispatchIfNecessary(() =>
            { tbxResult.Text += "\n" + result; });
            begin = DateTime.Now;
        }
    }
}
