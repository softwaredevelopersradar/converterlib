﻿using System;
using System.IO.Ports;
using System.Threading;

namespace ConverterLib
{
    public class ComPort
    {
        private Thread _readingThread;
        private SerialPort _port;
        private const byte BufferMaxSize = 255;

        public event EventHandler<byte[]> OnReadBytes;
        public event EventHandler<byte[]> OnWriteBytes;
        public event EventHandler OnOpenPort;
        public event EventHandler OnClosePort;
        public event EventHandler<Exception> OnException;

        public bool OpenPort(string portName, int baudRate)
        {
            return OpenPort(portName, baudRate, Parity.None, 8, StopBits.One);
        }

        public bool OpenPort(string portName, int baudRate, Parity parity, int dataBits, StopBits stopBits)
        {
            if (_port == null)
                _port = new SerialPort();
            
            if (_port.IsOpen)
                ClosePort();
            
            try
            {
                _port.PortName = portName;
                _port.BaudRate = baudRate;
                _port.Parity = parity;
                _port.DataBits = dataBits;
                _port.StopBits = stopBits;

                _port.Open();
                
                if (_readingThread != null)
                {
                    _readingThread.Abort();
                    _readingThread.Join(500);
                    _readingThread = null;
                }
                

                OnOpenPort?.Invoke(this, null);
                return true;
            }
            catch (Exception e)
            {
                OnClosePort?.Invoke(this, null);
                OnException?.Invoke(this, e);
                return false;
            }
        }


        public bool ClosePort()
        {
            try
            {
                _port.DiscardInBuffer();
                _port.DiscardOutBuffer();
            }
            catch (Exception)
            { }

            try
            {
                _port.Close();

                if (_readingThread != null)
                {
                    _readingThread.Abort();
                    _readingThread.Join(500);
                    _readingThread = null;
                }
                OnClosePort?.Invoke(this, null);
                return true;
            }
            catch (Exception e)
            {
                OnException?.Invoke(this, e);
                return false;
            }
        }
        
        protected byte[] WriteToComPort(byte[] writeBuffer, int timeout)
        {
            try
            {
                if (!_port.IsOpen)
                {
                    OnException?.Invoke(this, new Exception($"SerialPort is closed"));
                    return new byte[0];
                }

                _port.ReadTimeout = timeout;
                _port.DiscardOutBuffer();
                _port.DiscardInBuffer();

                _port.Write(writeBuffer, 0, writeBuffer.Length);
                OnWriteBytes?.Invoke(this, writeBuffer);
                
                var readBuffer = new byte[BufferMaxSize];
                var iReadByte = _port.Read(readBuffer, 0, BufferMaxSize);

                //waiting for the second byte
                _port.ReadTimeout = 1;
                try
                {
                    iReadByte += _port.Read(readBuffer, iReadByte, BufferMaxSize - iReadByte);
                }
                catch{ }


                if (iReadByte > 0)
                {
                    Array.Resize(ref readBuffer, iReadByte);
                    OnReadBytes?.Invoke(this, readBuffer);
                }    
                return readBuffer;
            }
            catch (Exception e)
            {  
                OnException?.Invoke(this, new Exception($"Timeout '{timeout}' ms expired"));
                return new byte[0];
            }
        }
        
    }
}
