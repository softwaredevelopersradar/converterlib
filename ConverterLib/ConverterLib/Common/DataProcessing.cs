﻿using System;
using System.Linq;

namespace ConverterLib
{
    internal static class DataProcessing
    {
        internal static byte CountChecksum(byte[] data)
        {
            byte result = 0;
            foreach (byte i in data)
            {
                result += i;
            }
            byte temp = Convert.ToByte(result ^ 0xFF);
            result = Convert.ToByte( temp + 1);

            return result;
        }


        internal static bool CheckResponse(byte[] response, byte[] dataToCheck)
        {
            if (response.Length == 0 || !Enumerable.SequenceEqual(response, dataToCheck))
                return false;

            return true;
        }


        internal static bool CheckResponse(byte[] response, byte firstByteToCheck)
        {
            if (response.Length == 0 || response[0] != firstByteToCheck)
                return false;

            return true;
        }
    }
}
