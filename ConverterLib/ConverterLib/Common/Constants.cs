﻿
namespace ConverterLib
{
    internal static class Constants
    {
        #region Relay
        public static readonly byte[] RangeFrom0To1GHzRequest = { 4, 5, 23, 0, 2, 1, 0 };
        public static readonly byte[] RangeFrom1To18GHzRequest = { 4, 5, 23, 0, 2, 0, 0 };
        public static readonly byte[] ControlSignalRequest = { 4, 5, 23, 0, 2, 2, 0 };

        public static readonly byte[] RangeFrom0To1GHzResponse = { 5, 4, 23, 0, 2, 1, 0 };
        public static readonly byte[] RangeFrom1To18GHzResponse = { 5, 4, 23, 0, 2, 0, 0 };
        public static readonly byte[] ControlSignalResponse = { 5, 4, 23, 0, 2, 2, 0 };
        #endregion


        #region Converter
        public static readonly byte[] HeaderBase = { 0x0, 0x0, 0x1, 0x0, 0x9, 0x0 };
        #endregion
    }
}
