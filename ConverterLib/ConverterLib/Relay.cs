﻿using System;

namespace ConverterLib
{
    public class Relay : ComPort
    {
        private int _awaiterTime = 400;
        public int AwaiterTime
        {
            get { return _awaiterTime; }
            set
            {
                if (_awaiterTime == value)
                    return;
                _awaiterTime = value;
            }
        }


        public bool SetRangeFrom0To1GHz()
        {
            return SendBase(Constants.RangeFrom0To1GHzRequest, Constants.RangeFrom0To1GHzResponse);
        }


        public bool SetRangeFrom1To18GHz()
        {
           return SendBase(Constants.RangeFrom1To18GHzRequest, Constants.RangeFrom1To18GHzResponse);
        }


        public bool EnableControlSignal()
        {
            return SendBase(Constants.ControlSignalRequest, Constants.ControlSignalResponse);
        }


        private bool SendBase(byte[] request, byte[] response)
        {
            try
            {
                var result = WriteToComPort(request, AwaiterTime);
                return DataProcessing.CheckResponse(result, response);
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
