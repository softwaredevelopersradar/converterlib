﻿using System;
using System.Linq;

namespace ConverterLib
{
    public enum Mode
    {
        Master = 3,
        Slave = 4
    }

    public class Converter : ComPort
    {
        private readonly Mode _mode;

        private int _awaiterTime = 400;
        public int AwaiterTime
        {
            get { return _awaiterTime; }
            set
            {
                if (_awaiterTime == value)
                    return;
                _awaiterTime = value;
            }
        }

        public Converter(Mode mode)
        {
            _mode = mode;
        }


        #region Commands
        
        public bool SetConverterFrequency(short frequency)
        {
            try
            { 
                byte[] data = BitConverter.GetBytes(frequency).Reverse().ToArray();

                var result = WriteToComPort(FormPacketForSend((byte)(_mode + 2), data), AwaiterTime);
                return DataProcessing.CheckResponse(result, 0xEE);
            }
            catch (Exception e)
            {
                return false;
            }
        }


        /// <summary>
        /// Has no response.
        /// </summary>
        public void SetCommutatorAntenna(byte antennaNum, byte attenuatorLoss)
        {
            var data = new byte[2] { antennaNum, attenuatorLoss};

            WriteToComPort(FormPacketForSend((byte)_mode, data), 0);
        }

        #endregion




        #region CreateArrayForSend

        private byte[] FormPacketForSend(byte mode, byte[] data)
        {
            var head = FormHeader(mode);
            var bSend = head.Concat(data).ToArray();

            var bodySum = new byte[1] { 0 };
            if (mode == (byte)(Mode.Master + 2) || mode == (byte)(Mode.Slave + 2))
            {
                bodySum[0] = DataProcessing.CountChecksum(data);
            }

            return bSend.Concat(bodySum).ToArray();
        }


        private byte[] FormHeader(byte mode)
        {
            byte[] header = Constants.HeaderBase;
            header[0] = mode;

            if (mode == (byte)(Mode.Master + 2) || mode == (byte)(Mode.Slave + 2))
            {
                var headerForSum = new byte[5];
                Array.Copy(header, 0, headerForSum, 0, headerForSum.Length);
               
                header[5] = DataProcessing.CountChecksum(headerForSum);
            }

            return header;
        }
        #endregion

    }
}
